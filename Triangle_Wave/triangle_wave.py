import scipy
import pylab
import sys
"""
Here's something you can code up if you get the chance.  Remember the python script that built up a square wave from its Fourier series components?  (1/n * cos(2*pi*n*f_0*t), n = 1, 3, 5, 7, ... )

Here are the coefficients for the Fourier series of another waveform (not a square wave).  Try coding this up and see what type of waveform it is:

b(n) = 8/(n**2) * (-1)**( (n-1)/2) for odd n
b(n) = 0 for even n


And b(n) is the coefficient for cos(2*pi*n*f_0*t) where f_0 is the fundamental frequency of the waveform.
"""
print sys.argv

# Script configuration
f_0 = 1E6       # Fundamental frequency
fs = 100E6
n_cycles = 1;            # number of wave periods in the time domain signal
if (len(sys.argv) == 2):
  max_order = int(sys.argv[1])
else:
  max_order= 10;            # Maximum index of Fourier Series to calculate
plot_all_components = 0; # Flag controlling whether plots of each component are gnerated
# end script configuration


k_max = n_cycles*(fs/f_0)
k = scipy.arange(k_max)


fourier_series = [];
for i in range(max_order):
  if ( (i % 2) == 1): # odd il
    # i is odd, calculate the Fourier Series component for the frquency of
    # i times the fundamental frquency
    component = 1.0 / 8/(i**2) * (-1)**((i-1)/2) * scipy.sin(2*scipy.pi*i*k*f_0/fs)
    fourier_series.append(component);
  else: # even i
    # Waves have zero Fourier Series components for even i
    component = scipy.zeros(k_max);
    fourier_series.append(component);

cumulative_sum = scipy.zeros(len(fourier_series[0]));
for i in range(len(fourier_series)):
  if ( (i % 2) == 1): # Skip the even (zero) fouier series components
    cumulative_sum += fourier_series[i];
    if (plot_all_components == 1):
      pylab.figure();
      pylab.subplot(211)
      pylab.plot(k, fourier_series[i]);
      pylab.title("Fouier Series Component, i=%d" % i)
      pylab.grid(1);
      pylab.legend();

      pylab.subplot(212)
      pylab.plot(k, cumulative_sum);
      pylab.title("Fourier Series Summation up to i=%d" % i)
      pylab.xlabel("Time Index")
      pylab.grid(1);
      pylab.legend();

# Generate a single plot showing all of the Fourier series components
pylab.figure();
for i in range(len(fourier_series)):
  if ( (i % 2) == 1): # Skip the even (zero) fouier series components
    pylab.plot(k, fourier_series[i]);

pylab.xlabel("Time Index")
pylab.title("Fourier Series Components")
pylab.grid(1)
pylab.show()

# Generate a single plot showing the cumulative sum of the Fourier Series
# components
pylab.figure();
pylab.plot(k, cumulative_sum);
pylab.xlabel("Time Index")
pylab.title("Cumulative Sum of Fourier Series Components up to i=%d" % ( max_order - 1))
pylab.grid(1)
pylab.show()
