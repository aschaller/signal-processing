import scipy
import pylab

# Explore modulation with time domain plots

# Script configuration
f_lo = 1E6
f_audio = 1E3
fs = 50E6
a = 0.1
k_max = 50000

# End script configuration
Ts = 1.0/fs

k = scipy.arange(k_max)

audio = scipy.sin(2*scipy.pi*f_audio*k*Ts)
lo = scipy.sin(2*scipy.pi*f_lo*k*Ts)

am1 = audio*lo
am2 = (1 - a*audio)*lo

pylab.figure()
pylab.plot(k*Ts, audio, label='audio')
pylab.plot(k*Ts, lo, label='lo')
pylab.legend(loc=0)
pylab.grid()

# Plot am1
pylab.figure()
pylab.subplot(211)
pylab.plot(k*Ts, am1, label='AM')
pylab.legend(loc=0)
pylab.grid()

pylab.subplot(212)
pylab.plot(k*Ts, audio, label='audio')
pylab.legend(loc=0)
pylab.grid()
pylab.show()

# Plot am2
pylab.figure()
pylab.subplot(211)
pylab.plot(k*Ts, am2, label='AM')
pylab.legend(loc=0)
pylab.grid()

pylab.subplot(212)
pylab.plot(k*Ts, audio, label='audio')
pylab.legend(loc=0)
pylab.grid()
pylab.show()

