import scipy
import pylab

"""
3) Form a signal that contains several periods of a tone whose
frequency is 1 kHz less than the maximum theoretical frequency that
you can represent (eg 1 kHz less than your answer to #2). Compare the
resulting plot to the plot for the 1 kHz tone created for #1; comment
on qualitative differences.
"""


# 1 kHz tone variables
k_max = 50
f1 = 21.05E3
fs = 44.1E3
fs2 = 500E3
k2_max = k_max*fs2/fs
T1 = 1.0/f1
Ts = 1.0/fs
Ts2 = 1.0/fs2

k = scipy.arange(k_max)
k2 = scipy.arange(k2_max)

y1 = scipy.sin(2*scipy.pi*f1*k*Ts)
y2 = scipy.sin(2*scipy.pi*f1*k2*Ts2)


pylab.plot(k*Ts, y1, 'o')
pylab.plot(k2*Ts2, y2)
pylab.grid()
pylab.show()
