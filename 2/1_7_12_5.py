import scipy
import pylab

"""
5) Form a 46.1 kHz signal sampled at 44.1 kHz. Compare the resulting
plot to #1 and #4 above. The signal that you just created has a
frequency that is only slightly greater (46.1kHz vs 45.1 kHz) than the
signal created for #4, but the apparent (from plot) ratio frequencies
is much greater. What's going on?  Experiment with different
frequencies, observing actual versus apparent. Tabulate your results
and see if you can recognize the pattern.
"""

# 46.1 kHz signal sampled at 44.1 kHz
k_max = 89
f = 46.1E3
fs = 44.1E3
T = 1.0/f
Ts = 1.0/fs

k = scipy.arange(k_max)

y = scipy.sin(2*scipy.pi/T*k*Ts)


pylab.plot(k*Ts, y, 'o-')
pylab.grid()
pylab.show()
