import scipy
import pylab

"""
1) Create a signal that contains several periods of a 1 kHz tone
sampled at 44.1 kHz. Observe the time domain samples; make sure that
the number of samples per waveform period makes sense.
"""

k_max = 89
f = 1E3
fs = 44.1E3
T = 1.0/f
Ts = 1.0/fs

k = scipy.arange(k_max)

y = scipy.sin(2*scipy.pi/T*k*Ts)


pylab.plot(k*Ts, y, '-o')
pylab.grid()
pylab.show()
