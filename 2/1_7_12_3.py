import scipy
import pylab

"""
3) Form a signal that contains several periods of a tone whose
frequency is 1 kHz less than the maximum theoretical frequency that
you can represent (eg 1 kHz less than your answer to #2). Compare the
resulting plot to the plot for the 1 kHz tone created for #1; comment
on qualitative differences.
"""


# 1 kHz tone variables
k_max = 89
f1 = 1E3
f2 = 21.05E3
fs = 44.1E3
T1 = 1.0/f1
T2 = 1.0/f2
Ts = 1.0/fs


k = scipy.arange(k_max)

y1 = scipy.sin(2*scipy.pi/T1*k*Ts)
y2 = scipy.sin(2*scipy.pi/T2*k*Ts)


pylab.plot(k*Ts, y1)
pylab.plot(k*Ts, y2, '-o')
pylab.grid()
pylab.show()
