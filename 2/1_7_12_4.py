import scipy
import pylab

"""
Now consider the case where we are running an ADC with a sample rate
of 44.1 kHz and there is no anti-aliasing filter. The sine_wave.py
script effectively samples the sin() function with no anti-aliasing
filter, so you can also use it to explore the following questions.

4) Form a 45.1 kHz signal sampled at 44.1 kHz. Compare the resulting
plot to #1 above and comment.
"""


# 45.1 kHz signal sampled at 44.1 kHz
k_max = 89
f = 18E3
fs = 40E3
T = 1.0/f
Ts = 1.0/fs

k = scipy.arange(k_max)

y = scipy.sin(2*scipy.pi/T*k*Ts)

pylab.figure()
pylab.plot(k*Ts, y, 'o-')
pylab.title("Frequency: %1.2f Hz, %1.2f Hz fs" % (f, fs))
pylab.grid()
pylab.show()
