import scipy
import pylab

# Form a sine wave with frequency 1 kHz, sample rate of 5 kHz

k_max = 1000
f = 1E3
fs = 50E3
T = 1.0/f
Ts = 1.0/fs

k = scipy.arange(k_max)

y = scipy.sin(2*scipy.pi/T*k*Ts)


pylab.plot(k*Ts, y, 'o-')
pylab.grid()
pylab.show()
