"""Design a lowpass filter using the windowed sinc method"""

import scipy
import scipy.signal
import pylab

# Script configuration
L = 1e4;  # FIR length,
theta_c = 2*scipy.pi * .0012  # Cutoff frequency, pi -> fs/2
use_windowing = 0;
# end script configuration

# Form the time axis 
if ( (L % 2) == 0):  # even length
  k = scipy.arange((-L+1)/2., (L+1)/2.);
else: # odd length
  k = scipy.arange(-L/2 + 1, L/2+1);

# Form the  filter's impulse response
h = theta_c / scipy.pi * scipy.sinc(k*theta_c / scipy.pi);

# Calculate the filter's frequency response
[w, H] = scipy.signal.freqz(h, 1);

# Apply a windowing function to the impulse response
w = scipy.hamming(len(h));
hw = h * w;
[w, Hw] = scipy.signal.freqz(hw, 1);
# Plot the impulse response
pylab.figure(1)
pylab.plot(k, h, 'o-')
if (use_windowing == 1):
  pylab.plot(k, hw, 'x-')
pylab.grid(1);
pylab.title('h(k)')
if (use_windowing == 1):
  pylab.legend(['Raw','Windowed'])


pylab.figure(2)
pylab.plot(w, 20*scipy.log10(abs(H)))
if (use_windowing == 1):
  pylab.plot(w, 20*scipy.log10(abs(Hw)))
pylab.ylabel('dB')
pylab.xlabel('theta, radians')
pylab.title('Magnitude Frequency Response')
pylab.grid(1);
if (use_windowing == 1):
  pylab.legend(['Raw','Windowed'])

pylab.show() 
