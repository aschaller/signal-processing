"""Apply a lowpass filter to a square wave."""
import scipy
import pylab

# Script Configuration

# The input to the signal is the square wave built up in the fourier_square_wave
# script. (so you have to run that first)

# Choose the cutoff frequency of the lowpass filter to correspond to the
# nth harmonic.  The 0.0012 value came from eyeballin the spacing between
# the 5th and 7th harmonic of a square wave generated with the following
# parameters (L=2**16, n_cycles = 5)
theta_c = 0.0005 * 2 * scipy.pi; 

# end script configuration

# Form the  filter's impulse response
h = theta_c / scipy.pi * scipy.sinc(k*theta_c / scipy.pi);

# Run the signal (cumulative_sum from fourier_square_wave.py script) through
# the lowpass filter:
y = scipy.signal.lfilter(h, 1, cumulative_sum)


# Time domain plot of filtered and unfiltered signals
pylab.figure();
pylab.plot(k, cumulative_sum, label="Filter Input");
pylab.plot(k, y, label="Filter Output");
pylab.grid(1);
pylab.title("Time Domain Filter input and outputs")

# Frequency domain plot of filtered and unfiltered signals

# Form a normalized frequency axis where 0 to 1 corresponds to 0 to pi
# or 0 to fs/2
f_axis = 1.0 * scipy.arange(L) / L
pylab.figure();
pylab.plot(f_axis, 20*scipy.log10(abs(scipy.fft(cumulative_sum))), label="Filter Input");
pylab.plot(f_axis, 20*scipy.log10(abs(scipy.fft(y))), label="Filter Output");
pylab.grid(1);
pylab.title("Power Spectrum of Filter input and outputs")
pylab.ylabel('dB')
pylab.xlabel('Normalized Frequency')


