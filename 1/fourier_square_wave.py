"""Build up a square wave in the time domain from its Fourier Series
   components
"""
import scipy
import pylab


# Script configuration
L = 2**16;    # Length of time domain sequence
n_cycles = 5;            # number of square wave periods in the time domain signal
max_order= 20;            # Maximum index of Fourier Series to calculate
plot_all_components = 0; # Flag controlling whether plots of each component are gnerated

# end script configuration


# Form the time axis
if ( (L % 2) == 0):  # even length
  k = scipy.arange((-L+1)/2., (L+1)/2.);
else: # odd length
  k = scipy.arange(-L/2 + 1, L/2+1);

# Form the different components of the Fourier Series representation of
# a square wave.  Each component is a scipy array containing a time domain
# signal.  'fourier_series' is a Python list, with each element being a
# component (scipy array)
fourier_series = [];
for i in range(max_order):
  if ( (i % 2) == 1): # odd i
    # i is odd, calculate the Fourier Series component for the frquency of
    # i times the fundamental frquency
    component = 1.0 / i * scipy.sin(2*scipy.pi*i*k*n_cycles/L)
    fourier_series.append(component);
  else: # even i
    # Square waves have zero Fourier Series components for even i
    component = scipy.zeros(L);
    fourier_series.append(component);

# Visualize the combination of the Fourier Series fourier_series by generating
# a series of figures, one for each Fourier Series component.  In the
# top plot, plot the Fourier series component.  in the bottom plot,
# plot the cumulative sum of the Fourier Series component up to and including
# the current one.

cumulative_sum = scipy.zeros(len(fourier_series[0]));
for i in range(len(fourier_series)):
  if ( (i % 2) == 1): # Skip the even (zero) fouier series components
    cumulative_sum += fourier_series[i];
    if (plot_all_components == 1):
      pylab.figure();
      pylab.subplot(211)
      pylab.plot(k, fourier_series[i]);
      pylab.title("Fouier Series Component, i=%d" % i)
      pylab.grid(1);
      pylab.legend();

      pylab.subplot(212)
      pylab.plot(k, cumulative_sum);
      pylab.title("Fourier Series Summation up to i=%d" % i)
      pylab.xlabel("Time Index")
      pylab.grid(1);
      pylab.legend();

# Generate a single plot showing all of the Fourier series components
pylab.figure();
for i in range(len(fourier_series)):
  if ( (i % 2) == 1): # Skip the even (zero) fouier series components
    pylab.plot(k, fourier_series[i]);

pylab.xlabel("Time Index")
pylab.title("Fourier Series Components")
pylab.grid(1)
pylab.show()

# Generate a single plot showing the cumulative sum of the Fourier Series
# components
pylab.figure();
pylab.plot(k, cumulative_sum);
pylab.xlabel("Time Index")
pylab.title("Cumulative Sum of Fourier Series Components up to i=%d" % ( max_order - 1))
pylab.grid(1)
pylab.show()
