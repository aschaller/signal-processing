import scipy

def dtft(x, theta):
	"""
	Calculate DTFT at a single frequency.
	"""
	X = 0
	for k in range(len(x)):
		X += x[k]*scipy.exp(-1j*k*theta)
	
	return X

