import scipy
import pylab
import sys
import dtft1

# Script configuration
signal_idx = 3
l_zero_pad = 0
# End script configuration

# Define the signal, based on signal_idx, from one of the predefined signals
if (signal_idx == 0):
  # A DC signal
  L = 10;
  x1 = scipy.ones(10)
elif (signal_idx == 1):
  # One period of a square wave, 10 samples per period
  L = 10;
  n_cycles = 1
  x = scipy.array(([1]*5+[-1]*5)*n_cycles)
elif (signal_idx == 2):
  # Generate a tone:
  #  f_sig is normalized frequency (f_sig = 1.0 corresponds to f_sig = fs)
  #  Generate the time index vector, k, so there are n_cycles of f_sig in the signal
  # Implicit f_s = T = 1
  f_sig = 0.100;     
  n_cycles = 4;  
  L = int(n_cycles / f_sig);
  k = scipy.arange(L);
  x = scipy.cos(2*scipy.pi*f_sig*k)
elif (signal_idx == 3):
  # Generate two tones
  f_sig1 = 0.340;
  f_sig2 = 0.100;
  n_cycles = 19;
  
  L = int(n_cycles / f_sig1);
  
  # Hard code L in attempt to place one of the signals at a half bin spacing
  # L = 124;
  k = scipy.arange(L)
  
  x1 = scipy.cos(2*scipy.pi*f_sig1*k)
  x2 = scipy.sin(2*scipy.pi*f_sig2*k)
  
  x = x1 + x2
  
else:
  print("signal_idx = %d is undefined" % signal_idx);

x = scipy.append(x, scipy.zeros(l_zero_pad))

# Define the (normalized) frequency samples at which DTFT will be evaluated
theta = scipy.arange(len(x)) * 2*scipy.pi/len(x)

# X will hold the DTFT of x
X = []

# Evaluate DTFT at each value of theta
for k in range(len(x)):
	X = scipy.append(X, dtft1.dtft(x, theta[k]))
        if (0):
	  print ("k=%2d theta = %2.2f  X(k): Re %2.2f Im %2.2f abs %2.1f" %
            (k,
             theta[k] / 2 / scipy.pi,
             scipy.real(X[k]),
             scipy.imag(X[k]),
             abs(X[k]),
            ))

real = scipy.real(X)
imag = scipy.imag(X)
mag = abs(X)

# Calc max, min y-axis values for DTFT plotting
y_max = max(mag)
y_min = - y_max;

style = 'o-'
# Plot time domain signal
pylab.figure()
pylab.plot(x, 'o-');
pylab.xlabel('Samples')
pylab.ylabel('x(k)')
pylab.grid()
pylab.title("Time Domain Waveform, signal_idx = %d" % signal_idx)

# Form real vs. frequency plot
pylab.figure()
pylab.subplot(311)
pylab.plot(theta/2/scipy.pi, real, style, label='Real')
pylab.title('DTFT')
pylab.ylabel('Real')
pylab.legend(loc=0)
pylab.grid()
v = scipy.array(pylab.axis());
v[2] = y_min; v[3] = y_max; pylab.axis(v);

# Form imaginary vs. frequency plot
pylab.subplot(312)
pylab.plot(theta/2/scipy.pi, imag, style, label='Imag')
#pylab.title('Imaginary vs. frequency')
pylab.ylabel('Imag')
pylab.legend(loc=0)
pylab.grid()
v = scipy.array(pylab.axis());
v[2] = y_min; v[3] = y_max; pylab.axis(v);

# Form magnitude vs. frequency plot
pylab.subplot(313)
pylab.plot(theta/2/scipy.pi, mag, style, label='Magnitude')
#pylab.title('Magnitude vs. Frequency')
pylab.legend(loc=0)
pylab.ylabel('Magnitude')
pylab.xlabel('Normalized Frequency (fs/2 -> 1)')
pylab.grid()
v = scipy.array(pylab.axis());
v[2] = y_min; v[3] = y_max; pylab.axis(v);
pylab.show()
